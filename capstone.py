from abc import ABC, abstractclassmethod
class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass
    def addRequest(self):
        pass
    def checkRequest(self):
        pass
    def addUser(self):
        pass
class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    # setter
    def set_fistName(self,firstName):
        self._firstName = firstName
    def set_LastName(self,lastName):
        self._lastName = lastName
    def set_email(self,email):
        self._email = email
    def set_department(self,department):
        self._department = department
    # getter
    def get_fistName(self):
        print(f"{self._firstName}")
    def get_LastName(self):
        print(f"{self._lastName}")
    def get_email(self):
        print(f"{self._email}")
    def get_department(self):
        print(f"{self._department}")
    # methods
    def getFullName(self):
        print(f"{self._firstName} {self._lastName}")
    def addRequest(self):
        print("Request has been added")
    def checkRequest(self):
        print("Request has been checked")
    def addUser(self):
        print("User has been added")
    def login(self):
        print(f"{self.set_email} has logged in")
    def logout(self):
        print(f"{self.set_email} has logged out")
class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []
    # setter
    def set_fistName(self,firstName):
        self._firstName = firstName
    def set_LastName(self,lastName):
        self._lastName = lastName
    def set_email(self,email):
        self._email = email
    def set_department(self,department):
        self._department = department
    def set_department(self,member):
        self._members = member
    # getter
    def get_fistName(self):
        print(f"{self._firstName}")
    def get_LastName(self):
        print(f"{self._lastName}")
    def get_email(self):
        print(f"{self._email}")
    def get_department(self):
        print(f"{self._department}")
    def get_members(self):
        return self._members
    # methods
    def getFullName(self):
        print(f"{self._firstName} {self._lastName}")
    def addRequest(self):
        print("Request has been added")
    def checkRequest(self):
        print("Request has been checked")
    def addUser(self):
        print("User has been added")
    def login(self):
        print(f"{self.set_email} has logged in")
    def logout(self):
        print(f"{self.set_email} has logged out")
    def addMember(self,employee):
        self._members.append(employee)
    
class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    # setter
    def set_fistName(self,firstName):
        self._firstName = firstName
    def set_LastName(self,lastName):
        self._lastName = lastName
    def set_email(self,email):
        self._email = email
    def set_department(self,department):
        self._department = department
    # getter
    def get_fistName(self):
        print(f"{self._firstName}")
    def get_LastName(self):
        print(f"{self._lastName}")
    def get_email(self):
        print(f"{self._email}")
    def get_department(self):
        print(f"{self._department}")
    # methods
    def getFullName(self):
        print(f"{self._firstName} {self._lastName}")
    def addRequest(self):
        print("Request has been added")
    def checkRequest(self):
        print("Request has been checked")
    def addUser(self):
        print("User has been added")
    def login(self):
        print(f"{self.set_email} has logged in")
    def logout(self):
        print(f"{self.set_email} has logged out")

class Request():
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = "Pending"
    # setter
    def set_name(self,name):
        self._name = name
    def set_LastName(self,requester):
        self._requester = requester
    def set_dateRequested(self,dateRequested):
        self._dateRequested = dateRequested
    def set_status(self,status):
        self._status = status
    # getter
    def get_name(self):
        print(f"{self._name}")
    def get_reuester(self):
        print(f"{self._requester}")
    def get_dateRequested(self):
        print(f"{self._dateRequested}")
    def get_status(self):
        print(f"{self._status}")
    # method
    def updateRequest(self, newName, newDate):
        self._name = newName
        self._dateRequested = newDate
    def closeRequest(self):
        print(f"Resquest {self._name} has been closed")
    def cancelRequest(self, ):
        print(f"Request {self._name} has been canceled")

# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
    
for indiv_emp in teamLead1.get_members():
    indiv_emp.getFullName()

req2.set_status("closed")
req2.closeRequest()